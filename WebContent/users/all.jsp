<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Enroll a new user</title>
</head>

<body>
	<h3>すべてのユーザー情報</h3>
	<table style="width:100%; border-style:solid; border-collapse: collapse">
		<tr>
			<th style="border-style:solid;">ID</th>
			<th style="border-style:solid;">氏名</th>
			<th style="border-style:solid;">住所</th>
			<th style="border-style:solid;">年齢</th>
			<th style="border-style:solid;">就職</th>
			<th style="border-style:solid;">削除</th>
		</tr>
		<s:iterator value="userList">
			<tr>
				<td style="border-style:solid; border-width:thin"><s:property value="id" /></td>
				<td style="border-style:solid; border-width:thin"><s:property value="name" /></td>
				<td style="border-style:solid; border-width:thin"><s:property value="address" /></td>
				<td style="border-style:solid; border-width:thin"><s:property value="age" /></td>
				<td style="border-style:solid; border-width:thin"><s:property value="work" /></td>
				<td style="border-style:solid; border-width:thin"><a href="delete?id=<s:property value='id' />">削除</a></td>
			</tr>
		</s:iterator>
	</table>
	
	<p><a href="<s:url action='../index' />" > Return </a>
</body>
</html>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Enroll successful</title>
</head>

<body>
<h3> User registration successful </h3>

<p>You enroll information: </p>
<hr>
ID:&nbsp;<s:property value="usr.id" />
氏名:&nbsp; <s:property value="usr.name" />
住所:&nbsp;<s:property value="usr.address"/>
年齢:&nbsp;<s:property value="usr.age"/>

就職:&nbsp;<s:property value="usr.work"/>
<p><a href="<s:url action='../index' />" > Return </a>
</body>
</html>
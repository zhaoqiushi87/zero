<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>User deletion successful</title>
</head>

<body>
<h3> ユーザーが削除されました。 </h3>

<p><a href="<s:url action='../index' />" > Return </a>
</body>
</html>
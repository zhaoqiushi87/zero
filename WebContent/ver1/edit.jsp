<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.List" %>
<%@ page import="com.hello.bean.busi.SysinfoOperator" %>
<%@ page import="com.hello.sysinfo.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Edit</title>

<script type="text/javascript">

</script>

</head>
<body>
<%

String idparam = request.getParameter("sysId");


// SELECT ID, NAME, PASS, SEX, MEMO FROM SYS_INFO WHERE NAME = 'Name1' and SEX = '2'

//在查询列表里 没有通过id进行查询，查询出来的结果有id显示出来，因为要通过这个带下划线的id点击它进行那一页内容的编辑，
//最后点更新按钮.

String sql = "SELECT ID, NAME, PASS, SEX, MEMO FROM SYS_INFO WHERE ID = '" 
		+ idparam + "'";
System.out.println("sql : " + sql);

SysinfoOperator dor = new SysinfoOperator();
List<SysInfo> siList = dor.querySysInfo(sql);
SysInfo si = siList.get(0);

int id = si.getId();
String name = si.getName();
String password = si.getPass();
String sex = si.getSex().equals("1") ? "男" : "女";
String memo = si.getMemo();

%>

<br><br>
情報をご入力ください：
<br><br>
<form name="form1" method="post" action="update.jsp">

<input type="hidden" name="id" value="<%= id %>"/>

<table>

<tr>
<td>ID</td>
<td><%= id %></td>
</tr>

<tr>
<td>名前</td>
<td><input type="text" name="name" value="<%= name %>"/></td>
</tr>

<tr>
<td>パスワード</td>
<td><input type="text" name="password" value="<%= password %>"/></td>
</tr>

<tr>
<td>Sex</td>

<td><%= sex%></td>
<td><input type="radio" name="sex" value="1" checked/>男
<input type="radio" name="sex" value="2"/>女</td>
</tr>

<tr>
<td>Memo</td>
<td><input type="text" name="memo" value="<%= memo %>"/></td>
</tr>


</table>
<input type="submit" value="更新"/>


</form>

</body>
</html>
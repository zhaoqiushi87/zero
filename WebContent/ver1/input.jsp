<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Input</title>

<script type="text/javascript">

function checkItem() {
	var uname = document.form1.username.value;
	
	var pass1 = document.form1.password.value;
	var pass2 = document.form1.password2.value;
	
	if (uname == null || uname.length == 0) {
		
		alert("名前項目に入力してください。");
		return false;
	}
	
	if (pass1 != pass2) {
		alert("確認パスワードとパスワードが違う。");
		return false;
	}
	
	return true;
}

</script>

</head>
<body>
<br><br>
情報をご入力ください：
<br><br>
<form name="form1" method="post" action="nextp.jsp" onsubmit="return checkItem();">

<table>

<tr>
<td>名前</td>
<td><input type="text" name="username" value=""/></td>
</tr>

<tr>
<td>パスワード</td>
<td><input type="password" name="password" value=""/></td>
</tr>

<tr>
<td>確認パスワード</td>
<td><input type="password" name="password2"/></td>
</tr>

<tr>
<td>性別</td>
<td>
<input type="radio" name="sex" value="1" />男
<input type="radio" name="sex" value="2"/>女</td>
</tr>

</table>

<br>

<input type="submit" value="登録"/>

<input type="reset" value="クリア"/>

</form>

</body>
</html>
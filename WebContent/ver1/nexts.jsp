<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.List" %>
<%@ page import="com.hello.bean.busi.SysinfoOperator" %>
<%@ page import="com.hello.sysinfo.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Next</title>
</head>
<body>

<%

String username = request.getParameter("username111");

if (username != null) {
	//username = new String(username.getBytes("ISO-8859-1"), "UTF-8");
}

String sex = request.getParameter("sex11");

if (sex != null) {
	//sex = new String(sex.getBytes("ISO-8859-1"), "UTF-8");
}

//SELECT 列名称 FROM 表名称
//SELECT * FROM 表名称
//SELECT LastName,FirstName FROM Persons WHERE City LIKE 'N%'
// SELECT ID, NAME, PASS, SEX, MEMO FROM SYS_INFO WHERE NAME = 'Name1' and SEX = '2'
//制作select表单-接受请求request提取查询内容-写SQL语句在数据库里进行查询-数据库里同时进行list查询for循环-最后显示查询结果。
//链接下一个edit更新那页-


String sql = "SELECT ID, NAME, PASS, SEX, MEMO FROM SYS_INFO WHERE 1=1 ";
if(username != null && !username.isEmpty()) {
	sql = sql + "and NAME like '%" + username + "%' ";
}

System.out.println("sql : " + sql);
%>

検索結果表示：
<br><br>
<table>

<tr>
<th width="50">No</th>
<th width="50">ID</th>
<th width="50">名前</th>
<th width="100">パスワード</th>
<th width="50">性別</th>
<th width="200">メモ</th>
<th width="50">削除</th>
</tr>
<%
SysinfoOperator dor = new SysinfoOperator();
List<SysInfo> siList = dor.querySysInfo(sql);
// for (SysInfo si : siList) {
for (int i = 0; i < siList.size(); i++) {
	SysInfo si = siList.get(i);
	int id = si.getId();
	String name = si.getName();
	String pass = si.getPass();
	String gender = si.getSex().equals("1") ? "男" : "女";
	String memo = si.getMemo();
%>

<tr>
<td align="center"><%=i+1%></td>
<td align="center"><a href="edit.jsp?sysId=<%=id%>"><%=id%></a></td>
<td align="center"><%=name%></td>
<td align="center"><%=pass%></td>
<td align="center"><%=gender%></td>
<td align="center"><%=memo%></td>
<td align="center"><a href="delete.jsp?sysId=<%=id%>">delete</a></td>
</tr>
<%
}
%>

</table>

<a href="index.jsp">Home</a><br><br>

</body>
</html>
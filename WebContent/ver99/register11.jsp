<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Insert title here</title>
</head>
<script type="text/javascript">

function checkItem() {
	var uname = document.form1.username.value;
	
	var pass1 = document.form1.password.value;
	var pass2 = document.form1.password2.value;
	
	if (uname == null || uname.length == 0) {
		
		alert("名前項目に入力してください。");
		return false;
	}
	
	if (pass1 != pass2) {
		alert("確認パスワードとパスワードが違う。");
		return false;
	}
	
	return true;
}

</script>
<body>
<h3> 登録 </h3>

<s:form action="register">
<s:textfield name="usr.name" label="氏名"/>
<s:textfield name="usr.password1" label="パスワード"/>
<s:textfield name="usr.password2" label="確認パスワード"/>
<s:textfield name="usr.sex" label="性別"/>

<s:submit/>
<s:reset/>
</s:form>


<p><a href="<s:url action='../index' />" > Return </a>
</body>
</html>
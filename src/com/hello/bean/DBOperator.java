package com.hello.bean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.hello.sysinfo.SysInfo;


/**
 * DB Operator
 * @author Admin
 *
 */
public class DBOperator {

	/**
	 * DB connection object
	 */
	private Connection qryCon;
	
	/**
	 * DB statement object
	 */
	private Statement qryStt;

	/**
	 * common DBQuery Method
	 * @param sql the sql statement for execute(query)
	 * @return the Result Set for query to call method 
	 */
	public ResultSet executeQuery(String sql) {

		ResultSet qryRes = null;

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			qryCon = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/asys?useUnicode=true&characterEncoding=UTF-8&relaxautoCommit=true&serverTimezone=GMT",
					"sysadmin", "trex2018");

			qryStt = qryCon.createStatement();
			qryRes = qryStt.executeQuery(sql);

		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}

		return qryRes;
	}

	/**
	 * release the DB resource 
	 */
	protected void closeSession() {
		try {

			if (qryStt != null) {
				qryStt.close();
			}

			if (qryCon != null) {
				qryCon.close();
			}

		} catch (Exception e) {

		}
	}

	public int executeUpdate(String sql) {
		return executeUpdate(sql, -1);
	}
	/**
	 * common DBQuery Method
	 * @param sql the sql statement for execute(insert,update,delete) 
	 * @return the Result for execute to call method 
	 */
	public int executeUpdate(String sql, int option) {

		int result = 0;

		Connection con;
		Statement stt;
		ResultSet rs = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/asys?useUnicode=true&characterEncoding=UTF-8&relaxautoCommit=true&serverTimezone=GMT",
					"sysadmin", "trex2018");
			// System.out.println("DB OKOK");
			stt = con.createStatement();
			if(option == -1) {
				result = stt.executeUpdate(sql);
			} else {
				result = stt.executeUpdate(sql, option);
			    rs = stt.getGeneratedKeys();
			    if (rs.next()) {
			    	result = rs.getInt(1);
			    } 
			}


		    
			stt.close();
			con.close();

		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}

		return result;

	}

}

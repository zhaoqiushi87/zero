package com.hello.bean;

import java.util.ArrayList;
import java.util.List;

import com.hello.test.TestBean;

/**
 * 
 * @author Admin
 *
 */
public class People {

	/**
	 * 名前
	 */
	private String name;

	/**
	 * 年齢
	 */
	private int age;

	private String sex;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String toString() {

		String str = "名前:" + this.name + " ~ " + "年齢:" + this.age + " ~ " + "性別:" + this.sex;

		return str;
	}

	public static void main(String[] args) {

		List<People> listBean = new ArrayList<People>();

		People ppppppp = new People();

		ppppppp.age = 12;
		ppppppp.setName("AAAAA");
		ppppppp.setSex("F");

		listBean.add(ppppppp);

		System.out.println(listBean.get(0).toString());

	}
}

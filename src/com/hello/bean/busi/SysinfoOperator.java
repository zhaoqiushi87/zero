package com.hello.bean.busi;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.hello.bean.DBOperator;
import com.hello.sysinfo.SysInfo;

public class SysinfoOperator extends DBOperator {

	public List<SysInfo> querySysInfo(String sql) {

		List<SysInfo> siList = new ArrayList<SysInfo>();

		ResultSet res = null;

		try {

			res = executeQuery(sql);

			while (res.next()) {
				SysInfo si = new SysInfo();

				si.setId(res.getInt("ID"));
				si.setName(res.getString("NAME"));
				si.setPass(res.getString("PASS"));
				si.setSex(res.getString("SEX"));
				si.setMemo(res.getString("MEMO"));

				siList.add(si);
			}

			res.close();
			closeSession();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return siList;
	}

	/**
	 * test method
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		SysinfoOperator db = new SysinfoOperator();

		String sql = "SELECT ID, NAME, PASS, SEX, MEMO FROM SYS_INFO ";

		List<SysInfo> list = db.querySysInfo(sql);

		for (SysInfo info : list) {
			System.out.println(info.getName());
		}
	}



public SysInfo login(String userid, String password) throws SQLException {
	
	ResultSet res;
	
	String sql = "SELECT ID, NAME, PASS, VIP FROM SYS_INFO where ID=" + userid + " and PASS='" + password + "'";
	System.err.println(sql);
	res = executeQuery(sql);

		SysInfo si = null;

		while (res.next()) {
			si = new SysInfo();
			si.setId(res.getInt("ID"));
			si.setName(res.getString("NAME"));
			si.setName(res.getString("PASS"));
			si.setVip("1".equals(res.getString("VIP")));
			break;
		}

		res.close();
	
	return si;

}
}
package com.hello.test;

import java.util.ArrayList;
import java.util.List;

import com.hello.bean.People;

public class TestBean {

	public void test1() {

		People jb1 = new People();

		jb1.setAge(12);
		jb1.setName("Liu");
		jb1.setSex("M");

		People jb2 = new People();

		jb2.setAge(56);
		jb2.setName("Wang");
		jb2.setSex("F");

		System.out.println(jb1.toString());
		System.out.println(jb2.toString());
	}

	public void test2() {

		List<People> listBean = new ArrayList<People>();

		People liu = new People();
		liu.setAge(12);
		liu.setName("Liu");
		liu.setSex("M");

		People wang = new People();
		wang.setAge(56);
		wang.setName("Wang");
		wang.setSex("F");

		People li = new People();
		li.setAge(9);
		li.setName("li");
		li.setSex("F");

		listBean.add(liu);
		listBean.add(wang);
		listBean.add(li);

		for (int i = 0; i < listBean.size(); i++) {

			People ppp = listBean.get(i);
			System.out.println(ppp.toString());
		}

		People ppp2 = listBean.get(1);
		System.out.println(ppp2.toString());
	}

	public void test3() {

		People[] peops = new People[3];

		People liu = new People();
		liu.setAge(13);
		liu.setName("Liu");
		liu.setSex("M");

		People wang = new People();
		wang.setAge(10);
		wang.setName("Wang");
		wang.setSex("F");

		People li = new People();
		li.setAge(59);
		li.setName("li");
		li.setSex("F");

		peops[0] = liu;
		peops[1] = wang;
		peops[2] = li;

//		for (int i = 0; i < peops.length; i++) {
//			People ppp = peops[i];
//			System.out.println(ppp.toString());
//		}

		for (People ppp : peops) {
			System.out.println(ppp.toString());
		}
	}

	public static void main(String[] args) {

		TestBean tb = new TestBean();
		tb.test2();

	}

}

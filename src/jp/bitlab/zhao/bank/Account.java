package jp.bitlab.zhao.bank;
import java.util.ArrayList;
import java.util.List;
import jp.bitlab.zhao.listener.EmailListener;
import jp.bitlab.zhao.listener.PhoneListener;
// balance余额，deposit存款，withdrawal提款，amount总额

public class Account {
	
	public interface BalanceListener {
		void onChange(int balance);
	}
	
	int accountNo;
	String name;
	int balance;
	
	List<BalanceListener> bls;
	
	public Account(int accountNo, String name) {
		this.accountNo = accountNo;
		this.name = name;
		this.balance = 0;
		this.bls = new ArrayList<Account.BalanceListener>();
	}
	
	public int getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(int accountNo) {
		this.accountNo = accountNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBalance() {
		return balance;
	}
	public void setBalance(int amount) {
		this.balance = amount;
	}
	
	public void deposit(int amount) {
		//balance = balance + amount;
		balance += amount;
	}
	
	public void withdrawal(int amount) {
		balance -= amount;
		for(BalanceListener l : bls) {
			l.onChange(this.balance);
		}
	}
	
	public void addListener(BalanceListener bl) {
		bls.add(bl);
	}
	
	public static void main(String[] args) {
		Account a1 = new Account(1, "zhao");
		
		a1.deposit(1000);
		a1.withdrawal(200);
		
		int phoneNumber;
		/**
		 * 
		 */
		phoneNumber = 234987;
		a1.addListener(new EmailListener("a@gmail.com"));
		a1.addListener(new PhoneListener(234987));

		a1.withdrawal(300);

		Account a2 = new Account(1, "he");
		a2.deposit(1000);
		a2.addListener(new EmailListener("aaaaa@gmail.com"));

		a2.withdrawal(100);
	}
}

package jp.bitlab.zhao.listener;

import jp.bitlab.zhao.bank.Account;
import jp.bitlab.zhao.bank.Account.BalanceListener;
//implements实施
public class EmailListener implements BalanceListener{
	private String emailAddress;

	public EmailListener(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	@Override
	public void onChange(int balance) {
		System.err.println(String.format("send email to %s, balance=%d", emailAddress, balance));
	}

}

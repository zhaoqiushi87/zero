package jp.bitlab.zhao.listener;

import jp.bitlab.zhao.bank.Account;
import jp.bitlab.zhao.bank.Account.BalanceListener;

public class PhoneListener implements BalanceListener {
	
	private long phoneNumber;
	
	public PhoneListener(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	@Override
	public void onChange(int balance) {
		System.err.println(String.format("send sms to %s, balance=%d", phoneNumber, balance));
	}
	
}

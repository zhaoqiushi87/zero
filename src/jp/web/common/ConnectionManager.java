package jp.web.common;

import java.io.IOException;
import java.io.Reader;

import com.ibatis.common.resources.Resources;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapClientBuilder;

import jp.web.users.User;
import jp.web.users.UserDao;
import jp.web.users.UserDaoIbatis;

public class ConnectionManager {
	static SqlMapClient sqlmapClient;
	static {
		// Create the SQLMapClient
		Reader reader;
		try {
			reader = Resources.getResourceAsReader("sql-maps-config.xml");
			sqlmapClient = SqlMapClientBuilder.buildSqlMapClient(reader);
		} catch (IOException e) {
			throw new RuntimeException("Failed to establish connection." + e);
		}
	}

	public static SqlMapClient getClient() {
		return sqlmapClient;
	}

	public static void main(String[] args) throws Exception {
		// Initialize dao
		UserDao manager = new UserDaoIbatis();

		// Create a new user to persist
		User user = new User();
		user.setId(1);
		user.setPassword("password");
		user.setName("Demo User");
		user.setAddress("Utopia");
		user.setAge("age");
		user.setWork("work");

		// Add the user
		manager.addUser(user, sqlmapClient);

		// Fetch the user detail
		User createdUser = manager.getUserById(1, sqlmapClient);
		System.out.println(createdUser.getAddress());

		// Lets delete the user
		manager.deleteUserById(1, sqlmapClient);
	}
}

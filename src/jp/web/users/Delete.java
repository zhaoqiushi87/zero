package jp.web.users;

import org.apache.struts2.ServletActionContext;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.opensymphony.xwork2.ActionSupport;

import jp.web.common.ConnectionManager;

public class Delete extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String execute() throws Exception {
		UserDaoIbatis dao = new UserDaoIbatis();

		SqlMapClient client = ConnectionManager.getClient();

		int id = Integer.parseInt(ServletActionContext.getRequest().getParameter("id"));

		dao.deleteUserById(id, client);

		return super.execute();
	}

}

package jp.web.users;

import java.util.ArrayList;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.opensymphony.xwork2.ActionSupport;

import jp.web.common.ConnectionManager;

public class Search extends ActionSupport {

	ArrayList<User> userList = new ArrayList<User>();

	private static final long serialVersionUID = 1L;

	@Override
	public String execute() throws Exception {
		UserDaoIbatis dao = new UserDaoIbatis();

		SqlMapClient client = ConnectionManager.getClient();

		userList.addAll(dao.findAll(client));

		return super.execute();
	}

	public ArrayList<User> getUserList() {
		return userList;
	}

	public void setUserList(ArrayList<User> userList) {
		this.userList = userList;
	}
}

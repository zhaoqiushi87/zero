package jp.web.users;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.opensymphony.xwork2.ActionSupport;

import jp.web.common.ConnectionManager;

public class UpdateUser extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private User usr;

	@Override
	public String execute() throws Exception {
		UserDaoIbatis dao = new UserDaoIbatis();

		SqlMapClient client = ConnectionManager.getClient();

		dao.addUser(usr, client);

		return super.execute();
	}

	public User getUsr() {
		return usr;
	}

	public void setUsr(User usr) {
		this.usr = usr;
	}

}

package jp.web.users;

import java.io.Serializable;

public class User implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;
	private String password;
	private String name;
	private String address;
	private String age;
	private String work;

	public User() {
		super();
	}

	public User(int id, String password, String name, String address, String age, String work) {
		super();
		this.id = id;
		this.password = password;
		this.name = name;
		this.address = address;
		this.age = age;
		this.work = work;

	}

	/**
	 * @return the age
	 */

	/**
	 * @param age the age to set
	 */

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getWork() {
		return work;
	}

	public void setWork(String work) {
		this.work = work;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}

package jp.web.users;

import java.util.List;

import com.ibatis.sqlmap.client.SqlMapClient;

public interface UserDao {

	User addUser(User user, SqlMapClient sqlmapClient);

	User getUserById(Integer id, SqlMapClient sqlmapClient);

	void deleteUserById(Integer id, SqlMapClient sqlmapClient);

	List<User> findAll(SqlMapClient sqlmapClient);

	User updUser(User user, SqlMapClient sqlmapClient);

}

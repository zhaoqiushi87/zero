package jp.web.users;

import java.util.List;

import com.ibatis.sqlmap.client.SqlMapClient;

public class UserDaoIbatis implements UserDao {
	@Override
	public User addUser(User user, SqlMapClient sqlmapClient) {
		try {
			int id = user.getId();
			sqlmapClient.insert("users.addUser", user);
			user = getUserById(id, sqlmapClient);
			return user;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public User updUser(User user, SqlMapClient sqlmapClient) {
		int iRet = 0;
		try {
			int id = user.getId();
			sqlmapClient.update("users.addUser", user);
			user = getUserById(id, sqlmapClient);
			return user;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}

	@Override
	public User getUserById(Integer id, SqlMapClient sqlmapClient) {
		try {
			User user = (User) sqlmapClient.queryForObject("users.getUserById", id);
			return user;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void deleteUserById(Integer id, SqlMapClient sqlmapClient) {
		try {
			sqlmapClient.delete("users.deleteUserById", id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<User> findAll(SqlMapClient sqlmapClient) {
		try {
			@SuppressWarnings("unchecked")
			List<User> users = (List<User>) sqlmapClient.queryForList("users.findAll");
			return users;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}